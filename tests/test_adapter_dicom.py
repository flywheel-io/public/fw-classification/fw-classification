import inspect
from unittest.mock import MagicMock
from fw_classification.adapters import dicom
import pytest


def test_dicom_adapter_preprocess(mocker):
    zip_file = mocker.patch("fw_classification.adapters.dicom.zipfile")
    dcm_mock = mocker.patch("fw_classification.adapters.dicom.DICOM")
    dcm_coll_mock = mocker.patch("fw_classification.adapters.dicom.DICOMCollection")
    get_header = mocker.patch("fw_classification.adapters.dicom.get_dicom_header")
    get_header.return_value = {"test"}
    zip_file.is_zipfile.return_value = True
    file_ = MagicMock()
    adapter = dicom.DicomAdapter(file_)
    out = adapter.preprocess()
    assert out == {"file": {"type": "dicom", "info": {"header": {"dicom": {"test"}}}}}
    dcm_mock.assert_not_called()
    dcm_coll_mock.from_zip.assert_called_once_with(adapter.file, force=True)
    get_header.assert_called_once()

    get_header.reset_mock()
    dcm_coll_mock.reset_mock()
    dcm_mock.reset_mock()
    zip_file.is_zipfile.return_value = False
    out = adapter.preprocess()
    assert out == {"file": {"type": "dicom", "info": {"header": {"dicom": {"test"}}}}}
    dcm_mock.assert_called_once()
    dcm_coll_mock.from_zip.assert_not_called()
    get_header.assert_called_once()


def test_dicom_adapter_preprocess_valid_args(mocker):
    """
    Test to ensure DICOM instantiation only uses valid arguments
    as defined by the DICOM class' __init__ method. This tripped us up.
    """
    # Retrieve the signature of the DICOM class
    dicom_signature = inspect.signature(dicom.DICOM)
    valid_param_names = [param.name for param in dicom_signature.parameters.values()]

    # Now we can mock the DICOM class
    zip_file = mocker.patch("fw_classification.adapters.dicom.zipfile")
    dcm_mock = mocker.patch("fw_classification.adapters.dicom.DICOM")
    mocker.patch("fw_classification.adapters.dicom.DICOMCollection")
    get_header = mocker.patch("fw_classification.adapters.dicom.get_dicom_header")
    get_header.return_value = {"test"}

    # Test case where the file is not a zip file, triggering DICOM instantiation
    zip_file.is_zipfile.return_value = False
    file_ = MagicMock()
    adapter = dicom.DicomAdapter(file_)

    # Call preprocess method
    adapter.preprocess()

    # Retrieve the arguments passed to the mock DICOM class instantiation
    called_args, called_kwargs = dcm_mock.call_args

    # Ensure all keyword arguments passed to the mock are valid
    for kwarg in called_kwargs:
        assert (
            kwarg in valid_param_names
        ), f"Unexpected argument {kwarg} passed to DICOM."

    # Check that the expected positional argument (file) was passed
    expected_positional_args = (adapter.file,)
    assert called_args == expected_positional_args

    # Verify that the force argument was correctly passed
    assert called_kwargs["force"] is True


def test_dicom_adapter_preprocess_unexpected_args(mocker):
    """
    Test to ensure that an error is raised if unexpected arguments
    are passed to the DICOM class, based on the real signature.
    """
    # Retrieve the real signature of the DICOM class
    dicom_signature = inspect.signature(dicom.DICOM)
    valid_param_names = [param.name for param in dicom_signature.parameters.values()]

    # Mock the DICOM class after obtaining its signature
    zip_file = mocker.patch("fw_classification.adapters.dicom.zipfile")
    dcm_mock = mocker.patch("fw_classification.adapters.dicom.DICOM")
    mocker.patch("fw_classification.adapters.dicom.DICOMCollection")
    get_header = mocker.patch("fw_classification.adapters.dicom.get_dicom_header")
    get_header.return_value = {"test"}

    # Simulate the DICOM class raising a TypeError when an unexpected argument is passed
    def mock_dicom_constructor(*args, **kwargs):
        for kwarg in kwargs:
            if kwarg not in valid_param_names:
                raise TypeError(f"Unexpected argument {kwarg}")
        return MagicMock()

    # Apply the mock behavior
    dcm_mock.side_effect = mock_dicom_constructor

    # Test case where the file is not a zip file
    zip_file.is_zipfile.return_value = False
    file_ = MagicMock()
    adapter = dicom.DicomAdapter(file_)

    # Simulate invalid argument being passed when instantiating the DICOM class
    with pytest.raises(TypeError, match="Unexpected argument invalid_arg"):
        # Manually trigger the mock to simulate passing an invalid argument
        dcm_mock(adapter.file, force=True, invalid_arg="invalid")
