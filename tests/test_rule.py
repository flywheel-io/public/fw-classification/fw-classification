"""Rule tests."""

import pytest
from dotty_dict import dotty

from fw_classification.classify import expressions, rule
from fw_classification.classify.utils import ProfileError

#################### Match class ######################


def test_match_class_init():
    exprs = [expressions.Is("field1", "val1"), expressions.Is("field2", "val2")]
    match = rule.Match(exprs)
    assert match.exprs == exprs
    assert match.match_type.value == "any"
    assert (
        repr(match)
        == "Match if Any are True: \n\t- field1 is val1\n\t- field2 is val2\n"
    )


def test_match_class_from_list():
    exprs = [
        {"and": [{"key": "field1", "is": "val1"}, {"key": "field2", "exists": True}]},
        {"key": "field3", "startswith": "test"},
    ]
    match, err = rule.Match.from_list(exprs, match_type=rule.MatchType.All)
    assert not err
    assert repr(match) == (
        "Match if All are True: \n"
        + "\t- field1 is val1\n\t and field2 exists"
        + "\n\t- field3 starts with test\n"
    )
    assert match.to_list() == exprs


def test_match_class_from_list_none():
    exprs = []
    out, errs = rule.Match.from_list(exprs)
    assert out is None
    assert len(errs) == 1
    err = errs[0]
    assert isinstance(err, ProfileError)
    assert err.msg == "Need at least one expression in match."
    assert err.component == "match"


@pytest.mark.parametrize(
    "i_dict, match_type, res",
    [
        (
            {"field1": "val1", "field2": "val2", "field3": "atest1"},
            rule.MatchType.All,
            False,
        ),
        (
            {"field1": "val1", "field2": "val2", "field3": "atest1"},
            rule.MatchType.Any,
            True,
        ),
        (
            {"field1": "val1", "field2": "val2", "field3": "test1"},
            rule.MatchType.All,
            True,
        ),
    ],
)
def test_match_class_evaluate(i_dict, match_type, res):
    exprs = [
        {"and": [{"key": "field1", "is": "val1"}, {"key": "field2", "exists": True}]},
        {"key": "field3", "startswith": "test"},
    ]
    match, err = rule.Match.from_list(exprs, match_type=match_type)
    assert not err
    i = dotty(i_dict)
    assert match.evaluate(i_dict) == res
    del i["field2"]
    assert not match.evaluate(i)


#################### Action class ######################


def test_action_class_init():
    exprs = [expressions.Set("field1", "val1"), expressions.Set("field2", "val2")]
    action = rule.Action(exprs)
    assert action.exprs == exprs
    assert (
        repr(action)
        == "Do the following: \n\t- set field1 to val1\n\t- set field2 to val2\n"
    )


def test_action_class_from_list():
    exprs = [{"key": "field1", "set": "val1"}, {"key": "field2", "set": "val2"}]
    action, errs = rule.Action.from_list(exprs)
    assert not errs
    assert (
        repr(action)
        == "Do the following: \n\t- set field1 to val1\n\t- set field2 to val2\n"
    )
    assert action.to_list() == exprs


def test_action_class_from_list_none():
    exprs = []
    out, errs = rule.Action.from_list(exprs)
    assert out is None
    assert not errs


def test_action_class_evaluate():
    exprs = [{"key": "field1", "set": "val1"}, {"key": "field2", "set": "val2"}]
    action, err = rule.Action.from_list(exprs)
    assert not err
    i_dict = dotty({})
    action.evaluate(i_dict)
    assert i_dict["field1"] == "val1"
    assert i_dict["field2"] == "val2"


#################### Rule class ######################


def test_rule_class_init():
    match_exprs = [expressions.Is("field1", "val1"), expressions.Is("field2", "val2")]
    match = rule.Match(match_exprs)
    action_exprs = [
        expressions.Set("field1", "val3"),
        expressions.Set("field2", "val4"),
    ]
    action = rule.Action(action_exprs)

    r = rule.Rule(match, action)
    assert r.match.exprs == match_exprs
    assert r.action.exprs == action_exprs
    assert r.match_type.value == "any"
    assert repr(r) == (
        "Match if Any are True: \n\t- field1 is val1\n\t- field2 is val2\n"
        + "\n"
        + "Do the following: \n\t- set field1 to val3\n\t- set field2 to val4\n"
    )


@pytest.mark.parametrize("match_type, str_rep", [("all", "All"), ("any", "Any")])
def test_rule_class_from_dict(match_type, str_rep):
    rule_dict = {
        "match": [
            {
                "and": [
                    {"key": "field1", "is": "val1"},
                    {"key": "field2", "exists": True},
                ]
            },
            {"key": "field3", "startswith": "test"},
        ],
        "action": [{"key": "field1", "set": "val1"}, {"key": "field2", "set": "val2"}],
        "match_type": match_type,
    }

    r, errs = rule.Rule.from_dict(rule_dict)
    assert not errs
    assert repr(r) == (
        f"Match if {str_rep} are True: \n"
        + "\t- field1 is val1\n\t and field2 exists"
        + "\n\t- field3 starts with test\n"
        + "\n"
        + "Do the following: \n\t- set field1 to val1\n\t- set field2 to val2\n"
    )
    assert r.to_dict() == rule_dict


def test_rule_class_from_dict_match_type_none():
    rule_dict = {
        "match": [{"key": "field2", "exists": True}],
        "action": [{"key": "field2", "set": "val2"}],
        "match_type": None,
    }
    r, errs = rule.Rule.from_dict(rule_dict)
    assert not errs
    assert repr(r) == (
        "Match if Any are True: \n"
        + "\t- field2 exists\n\n"
        + "Do the following: \n\t- set field2 to val2\n"
    )
    rule_dict["match_type"] = "any"
    assert r.to_dict() == rule_dict


def test_rule_class_from_dict_match_type_missing():
    rule_dict = {
        "match": [{"key": "field2", "exists": True}],
        "action": [{"key": "field2", "set": "val2"}],
    }
    r, errs = rule.Rule.from_dict(rule_dict)
    assert not errs
    assert repr(r) == (
        "Match if Any are True: \n"
        + "\t- field2 exists\n\n"
        + "Do the following: \n\t- set field2 to val2\n"
    )
    rule_dict["match_type"] = "any"
    assert r.to_dict() == rule_dict


def test_rule_class_from_dict_invalid_match_type():
    rule_dict = {
        "match": [
            {"key": "field3", "startswith": "test"},
        ],
        "action": [{"key": "field1", "set": "val1"}, {"key": "field2", "set": "val2"}],
        "match_type": "test",
    }
    r, errs = rule.Rule.from_dict(rule_dict)
    assert r is None
    assert len(errs) == 1
    assert errs[0].msg == "Unknown match_type test"


@pytest.mark.parametrize(
    "match, action, errs",
    [
        (
            [{"key": "field3", "startswith": "test"}],
            [{"key": "field1", "set": "val1"}],
            False,
        ),
        ([], [{"key": "field1", "set": "val1"}], True),
        ([{"key": "field3", "startswith": "test"}], [], False),
        ([], [], True),
    ],
)
def test_rule_class_from_dict_match_action_missing(match, action, errs):
    rule_dict = {"match": match, "action": action, "match_type": "all"}
    out, err = rule.Rule.from_dict(rule_dict)
    if errs:
        assert out is None
        assert len(err) == 1
        assert err[0].msg == "Need at least one expression in match."
        assert err[0].stack == ["match", "rule"]
    else:
        assert not err


def test_rule_class_from_dict_err_propagates():
    rule_dict = {"match": [{"key": "field3", "statswith": "test"}], "match_type": "all"}
    out, err = rule.Rule.from_dict(rule_dict)
    assert out is None
    assert len(err) == 1
    assert err[0].component == "expression"
    assert "Could not find implemented" in err[0].msg
    assert err[0].stack == ["expression", "match", "rule"]


@pytest.mark.parametrize(
    "i_dict, res, out",
    [
        (
            {"field1": "val1", "field2": "val2", "field3": "test1"},
            True,
            {"field1": "val3", "field2": "val4", "field3": "test1"},
        ),
        (
            {"field1": "val1", "field2": "val2", "field3": "atest1"},
            False,
            {"field1": "val1", "field2": "val2", "field3": "atest1"},
        ),
    ],
)
def test_rule_class_evaluate(i_dict, res, out):
    rule_dict = {
        "match": [
            {
                "and": [
                    {"key": "field1", "is": "val1"},
                    {"key": "field2", "exists": True},
                ]
            },
            {"key": "field3", "startswith": "test"},
        ],
        "action": [{"key": "field1", "set": "val3"}, {"key": "field2", "set": "val4"}],
        "match_type": "all",
    }

    r, err = rule.Rule.from_dict(rule_dict)
    assert not err
    i = dotty(i_dict)
    assert r.evaluate(i) == res
    assert i == dotty(out)


def test_rule_evaluate_no_action():
    rule_dict = {
        "match": [
            {"key": "field3", "startswith": "test"},
        ],
        "match_type": "all",
    }
    r, err = rule.Rule.from_dict(rule_dict)
    assert not err
    i_dict = {"field1": "val1", "field2": "val2", "field3": "test1"}
    i = dotty(i_dict)
    assert r.evaluate(i) is True
    assert dotty(i_dict) == i
