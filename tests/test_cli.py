import shlex

import pytest
from typer.testing import CliRunner

from fw_classification.cli import app


@pytest.fixture(scope="session")
def runner():
    runner = CliRunner()
    return runner


@pytest.fixture
def run(runner):
    def run_cmd(cmd: str):
        return runner.invoke(app, shlex.split(cmd))

    return run_cmd


def test_validate_success(mocker, run, tmp_path):
    profile_path = tmp_path / "profile.yml"
    profile_path.touch()
    profile = mocker.patch("fw_classification.cli.Profile")
    profile.return_value.errors = None
    result = run(f"validate {profile_path}")
    assert result.exit_code == 0
    assert "Success" in result.stdout


def test_validate_profile_cant_be_read(mocker, run, tmp_path):
    profile_path = tmp_path / "profile.yml"
    profile_path.touch()
    profile = mocker.patch("fw_classification.cli.Profile")
    profile.side_effect = ValueError("test")
    result = run(f"validate {profile_path}")
    assert "Could not load profile" in result.stdout
    assert result.exit_code != 0


def test_validate_profile_has_errs(mocker, run, tmp_path):
    profile_path = tmp_path / "profile.yml"
    profile_path.touch()
    profile = mocker.patch("fw_classification.cli.Profile")
    profile.return_value.errors = ["test"]
    result = run(f"validate {profile_path}")
    assert result.exit_code == 0
    assert "Found the following" in result.stdout


@pytest.mark.parametrize("out_file", ["", " -o "])
def test_classify_no_adapter(mocker, run, tmp_path, out_file):
    dcm = tmp_path / "dicom.zip"
    profile = tmp_path / "profile.yml"
    if out_file:
        out_file += str(tmp_path / "out.json")
    dcm.touch()
    profile.touch()
    json = mocker.patch("fw_classification.cli.json")
    run_mock = mocker.patch("fw_classification.cli.run_classification")
    result = run(f"run {profile} {dcm} {out_file}")
    run_mock.assert_called_once_with(profile, json.load.return_value)
    assert result.exit_code == 0
    assert "Loading input file" in result.stdout
    assert "Running classification" in result.stdout
    if out_file:
        assert "Success!" in result.stdout
