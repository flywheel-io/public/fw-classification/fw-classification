"""Tests for expressions."""

import pytest
from dotty_dict import dotty

from fw_classification.classify.expressions.expression import *  # pylint: disable=unused-wildcard-import,wildcard-import

############ Binary Expression tests ############


@pytest.mark.parametrize(
    "short,long,in_,out_",
    [
        (
            "fihd",
            "file.info.header.dicom",
            "$fihd.PatientName",
            "file.info.header.dicom.PatientName",
        ),
        (
            "fc",
            "file.classification",
            "$fc.Scan Orientation",
            "file.classification.Scan Orientation",
        ),
    ],
)
def test_alias_sub(short, long, in_, out_):
    variables = {short: long}
    expr = Is(in_, "Anonymized", variables=variables)
    assert expr.field == out_
    assert expr.value == "Anonymized"


def test_alias_not_exist():
    variables = {}
    expr = Is("$fihd.PatientName", "Anonymized", variables=variables)
    with pytest.raises(ValueError):
        _ = expr.field


def test_not_using_alias():
    variables = {"fihd": "file.info.header.dicom"}
    expr = Is("test", "test1", variables=variables)
    assert expr.field == "test"
    assert expr.value == "test1"


################# Match tests ###################


@pytest.mark.parametrize(
    "cls, args, kwargs, i_dict, matches",
    [
        (Contains, ("field", "value"), {}, {"field": ["value1", "value"]}, True),
        (Contains, ("field", "value"), {}, {"field": ["value1", "other"]}, False),
        (
            Contains,
            ("field", "field2"),
            {"resolve": True},
            {"field": ["value1", "value"], "field2": "value"},
            True,
        ),
        (
            Contains,
            ("field", "field2"),
            {"resolve": True},
            {"field": ["value1", "other"], "field2": "value"},
            False,
        ),
        (Exists, ("field", True), {}, {"field": "value"}, True),
        (Exists, ("field", False), {}, {"field": "other"}, False),
        (Exists, ("field", True), {}, {}, False),
        (Exists, ("field", False), {}, {"field": "other"}, False),
        (IsEmpty, ("field", True), {}, {"field": "not empty"}, False),
        (IsEmpty, ("field", True), {}, {"field": False}, False),
        (IsEmpty, ("field", True), {}, {"field": 0}, False),
        (IsEmpty, ("field", True), {}, {"field": ""}, True),
        (IsEmpty, ("field", True), {}, {"field": []}, True),
        (IsEmpty, ("field", True), {}, {"field": None}, True),
        (IsNumeric, ("field", False), {}, {"field": "other"}, True),
        (IsNumeric, ("field", True), {}, {"field": "other"}, False),
        (IsNumeric, ("field", True), {}, {"field": 0.2}, True),
        (IsNumeric, ("field", True), {}, {}, False),
        (IsNumeric, ("field", False), {}, {}, True),
        # GreaterThan + LessThan
        (GreaterThan, ("field", 1), {}, {"field": 2}, True),
        (GreaterThan, ("field", 1), {"approximate": 1}, {"field": 1}, True),
        (GreaterThan, ("field", 1), {"approximate": 1}, {"field": 1.01}, True),
        (GreaterThan, ("field", 1), {}, {"field": 0}, False),
        (
            GreaterThan,
            ("field", "field2"),
            {"resolve": True},
            {"field": 0, "field2": 1},
            False,
        ),
        (
            GreaterThan,
            ("field", "field2"),
            {"resolve": True},
            {"field": 2, "field2": 1},
            True,
        ),
        (LessThan, ("field", 1), {}, {"field": 0}, True),
        (LessThan, ("field", 1), {}, {"field": 2}, False),
        (
            LessThan,
            ("field", "field2"),
            {"resolve": True},
            {"field": 0, "field2": 1},
            True,
        ),
        (
            LessThan,
            ("field", "field2"),
            {"resolve": True},
            {"field": 2, "field2": 1},
            False,
        ),
        (Is, ("field", "value"), {}, {"field": "value"}, True),
        (Is, ("field", "value"), {}, {"field": "other"}, False),
        # Is approximate
        (Is, ("field", 0), {}, {"field": 0}, True),
        (Is, ("field", 0.01), {}, {"field": 0}, False),
        (Is, ("field", 0), {"approximate": 1}, {"field": 0}, True),
        (Is, ("field", 0.01), {"approximate": 1}, {"field": 0}, True),
        (Is, ("field", 0.1), {"approximate": 1}, {"field": 0}, False),
        (
            Is,
            ("field", "field2"),
            {"resolve": True},
            {"field": 0, "field2": 0},
            True,
        ),
        (
            Is,
            ("field", "field2"),
            {"resolve": True},
            {"field": 0.01, "field2": 0},
            False,
        ),
        (
            Is,
            ("field", "field2"),
            {"resolve": True, "approximate": 1},
            {"field": 0.01, "field2": 0},
            True,
        ),
        (
            Is,
            ("field", "field2"),
            {"resolve": True, "approximate": 1},
            {"field": 0.1, "field2": 0},
            False,
        ),
        (In, ("field", ["value", "value1"]), {}, {"field": "value"}, True),
        (In, ("field", ["value1", "value2"]), {}, {"field": "other"}, False),
        (
            In,
            ("field", "field2"),
            {"resolve": True},
            {"field": "value", "field2": ["value", "value1"]},
            True,
        ),
        (
            In,
            ("field", "field2"),
            {"resolve": True},
            {"field": "other", "field2": ["value1", "value2"]},
            False,
        ),
        (Regex, ("field", "^test"), {}, {"field": "test"}, True),
        (Regex, ("field", "^test"), {}, {"field": "atest"}, False),
        (Regex, ("field", "^test"), {"case_sensitive": True}, {"field": "Test"}, False),
        (Regex, ("field", "^test"), {"case_sensitive": False}, {"field": "Test"}, True),
        (Startswith, ("field", "test"), {}, {"field": "test"}, True),
        (Startswith, ("field", "test"), {}, {"field": "atest"}, False),
    ],
)
def test_match_expr_evaluate(cls, args, kwargs, i_dict, matches):
    d = dotty(i_dict)
    expr = cls(*args, **kwargs)
    assert expr.evaluate(d) == matches


@pytest.mark.parametrize(
    "cls, args, kwargs, i_dict, matches",
    [
        # LessThan expects a number, if value can't be caseted to numeric,
        # expect a False return
        (LessThan, ("field", 10), {}, {"field": None}, False),  # Nonetype
        (LessThan, ("field", 10), {}, {"field": False}, True),  # Translates to 0
        (LessThan, ("field", 10), {}, {"field": ""}, False),  # empty string
        (LessThan, ("field", 10), {}, {}, False),  # not in dict
        # LessThan expects a number
        (Is, ("field", 10), {}, {"field": None}, False),  # Nonetype
        (Is, ("field", 10), {}, {"field": False}, False),  # Bool
        (Is, ("field", 10), {}, {"field": ""}, False),  # empty string
        (Is, ("field", 10), {}, {}, False),  # not in dict
    ],
)
def test_match_expr_evaluate_value_mismatch(cls, args, kwargs, i_dict, matches):
    d = dotty(i_dict)
    expr = cls(*args, **kwargs)
    assert expr.evaluate(d) == matches


@pytest.mark.parametrize(
    "cls, args, kwargs, i_dict, matches",
    [
        (GreaterThan, ("field", 1), {}, {"field": "two"}, False),
        (LessThan, ("field", 1), {}, {"field": "two"}, False),
    ],
)
def test_match_expr_evaluate_warns(cls, args, kwargs, i_dict, matches, caplog):
    d = dotty(i_dict)
    expr = cls(*args, **kwargs)
    assert expr.evaluate(d) == matches
    assert caplog.record_tuples[-1][1] == 30
    assert "non-numeric" in caplog.record_tuples[-1][2]
    assert "field" in caplog.record_tuples[-1][2]


@pytest.mark.parametrize(
    "cls, args, kwargs, val",
    [
        (Contains, ("field", "value"), {}, "value is in field"),
        (Exists, ("field", True), {}, "field exists"),
        (Exists, ("field", False), {}, "field doesn't exist"),
        (GreaterThan, ("field", 1), {}, "field is greater than 1.0"),
        (Is, ("field", "value"), {}, "field is value"),
        (
            Is,
            ("field", "value"),
            {"approximate": 1},
            "field is value when rounding to 1 digits",
        ),
        (In, ("field", ["value1", "value2"]), {}, "field is in ['value1', 'value2']"),
        (LessThan, ("field", 1), {}, "field is less than 1.0"),
        (Regex, ("field", "^test"), {}, "field matches regex ^test"),
        (Startswith, ("field", "test"), {}, "field starts with test"),
    ],
)
def test_match_repr(cls, args, val, kwargs):
    expr = cls(*args, **kwargs)
    assert repr(expr) == val


@pytest.mark.parametrize(
    "cls, args, variables, kwargs, i_dict, matches",
    [
        (
            Contains,
            ("$f", "$f2"),
            {"f": "field", "f2": "field2"},
            {"resolve": True},
            {"field": ["value1", "value"], "field2": "value"},
            True,
        ),
        (
            GreaterThan,
            ("$f", "$f2"),
            {"f": "field", "f2": "field2"},
            {"resolve": True},
            {"field": 2, "field2": 1},
            True,
        ),
        (
            LessThan,
            ("$f", "$f2"),
            {"f": "field", "f2": "field2"},
            {"resolve": True},
            {"field": 0, "field2": 1},
            True,
        ),
        (
            Is,
            ("$f", "$f2"),
            {"f": "field", "f2": "field2"},
            {"resolve": True},
            {"field": 0, "field2": 0},
            True,
        ),
        (
            Is,
            ("$f", "$f2"),
            {"f": "field", "f2": "field2"},
            {"resolve": True, "approximate": 1},
            {"field": 0.01, "field2": 0},
            True,
        ),
        (
            In,
            ("$f", "$f2"),
            {"f": "field", "f2": "field2"},
            {"resolve": True},
            {"field": "value", "field2": ["value", "value1"]},
            True,
        ),
    ],
)
def test_match_expr_evaluate_variables(cls, args, variables, kwargs, i_dict, matches):
    d = dotty(i_dict)
    expr = cls(*args, **kwargs, variables=variables)
    assert expr.evaluate(d) == matches


############# Custom config tests and validation ################


def test_regex_custom_config():
    in_dict = {"key": "test", "regex": "test", "case_sensitive": False}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0

    in_dict = {"key": "test", "regex": "test", "case_sensitive": "my_val"}
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].msg == "Regex case-sensitive must be boolean, found 'my_val'"


def test_add_custom_config():
    in_dict = {"key": "test", "add": "test", "allow_duplicate": False}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0

    in_dict = {"key": "test", "add": "test", "allow_duplicate": "my_val"}
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].msg == "Add allow-duplicate must be boolean, found 'my_val'"


def test_is_approximate():
    in_dict = {"key": "test", "is": 0, "approximate": 1}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0

    in_dict = {"key": "test", "is": "test", "approximate": 1}
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].msg == "Value must be numeric if using approximate, found 'test'"

    in_dict = {"key": "test", "is": 0, "approximate": "test"}
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].msg == "Is: approximate must be int, found 'test'"


def test_greater_than_validation():
    in_dict = {"key": "test", "greater_than": 0, "approximate": 1}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0

    in_dict = {"key": "test", "greater_than": "test", "approximate": 1}
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].msg == "'greater_than' must be numeric value, found 'test'"

    in_dict = {"key": "test", "greater_than": "test", "approximate": 1, "resolve": True}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0


def test_less_than_validation():
    in_dict = {"key": "test", "less_than": 0, "approximate": 1}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0

    in_dict = {"key": "test", "less_than": "test", "approximate": 1}
    out, err = Expression.from_dict(in_dict)
    assert out is None
    assert err[0].msg == "'less_than' must be numeric value, found 'test'"

    in_dict = {"key": "test", "less_than": "test", "approximate": 1, "resolve": True}
    out, err = Expression.from_dict(in_dict)
    assert out
    assert len(err) == 0


################# Action tests ###################


@pytest.mark.parametrize(
    "cls, start, args, kwargs, out",
    [
        (Set, {}, ("field", "value"), {}, {"field": "value"}),
        (Add, {}, ("field", "value"), {}, {"field": ["value"]}),
        (Add, {}, ("field", ["value1", "value2"]), {}, {"field": ["value1", "value2"]}),
        (
            Add,
            {"field": ["test"]},
            ("field", "value"),
            {},
            {"field": ["test", "value"]},
        ),
        (Add, {"field": ["value"]}, ("field", "value"), {}, {"field": ["value"]}),
        (
            Add,
            {"field": ["value"]},
            ("field", "value"),
            {"allow_duplicate": True},
            {"field": ["value", "value"]},
        ),
    ],
)
def test_action_expr_evaluate(cls, start, args, kwargs, out):
    d = dotty(start)
    expr = cls(*args, **kwargs)
    expr.evaluate(d)
    assert d == dotty(out)


@pytest.mark.parametrize(
    "cls, args, val",
    [
        (Set, ("field", "value"), "set field to value"),
        (Add, ("field", "value"), "add value to field"),
    ],
)
def test_action_repr(cls, args, val):
    expr = cls(*args)
    assert repr(expr) == val


################## Unary tests ####################


def test_not_raises_when_len_gt_1():
    with pytest.raises(ValueError) as e:
        Not([Is("test", "test"), Is("test1", "test1")])
    assert e.value.args == (
        "Not block can only have 1 child element, found 2",
        "Try grouping with And or Or",
    )


@pytest.mark.parametrize(
    "cls, args, i_dict, res",
    [
        (
            And,
            ([Is("field1", "value1"), Is("field2", "value2")],),
            {"field1": "value1", "field2": "value2"},
            True,
        ),
        (
            And,
            ([Is("field1", "value1"), Is("field2", "value2")],),
            {"field1": "value1", "field2": "other"},
            False,
        ),
        (
            Or,
            ([Is("field1", "value1"), Is("field2", "value2")],),
            {"field1": "value1", "field2": "other"},
            True,
        ),
        (
            Or,
            ([Is("field1", "value1"), Is("field2", "value2")],),
            {"field1": "other", "field2": "other"},
            False,
        ),
        (Not, ([Is("field1", "value1")],), {"field1": "other"}, True),
        (Not, ([Is("field1", "value1")],), {"field1": "value1"}, False),
        (
            Not,
            ([Or([Is("field1", "value1"), Is("field2", "value2")])],),
            {"field1": "other", "field2": "other"},
            True,
        ),
    ],
)
def test_unary_evaluate(cls, args, i_dict, res):
    d = dotty(i_dict)
    expr = cls(*args)
    assert expr.evaluate(d) == res


@pytest.mark.parametrize(
    "cls, args, val",
    [
        (
            And,
            ([Is("field1", "value1"), Is("field2", "value2")],),
            "field1 is value1\n\t and field2 is value2",
        ),
        (
            Or,
            ([Is("field1", "value1"), Is("field2", "value2")],),
            "field1 is value1\n\t or field2 is value2",
        ),
        (
            Not,
            ([Is("field1", "value1")],),
            "not (\n\tfield1 is value1\n)",
        ),
        (
            Not,
            ([Or([Is("field1", "value1"), Is("field2", "value2")])],),
            "not (\n\tfield1 is value1\n\t or field2 is value2\n)",
        ),
    ],
)
def test_unary_repr(cls, args, val):
    expr = cls(*args)
    assert repr(expr) == val
