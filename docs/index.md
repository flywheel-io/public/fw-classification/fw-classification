# fw-classification

## Overview

`fw-classification` is a generalized file classifier for medical images.
At it's heart `fw-classification` is essentially an engine
that takes in a JSON document and applies a set of declarative rules to modify that JSON.

Updates to be made to metadata (passed in as JSON) are captured in a YML
file (referred as `profile` in the following)
which defined the updates to be applied.

Each profile defines [`block`](./fw-classification/profile.md#block) which contain
[`rule`](./fw-classification/profile.md#rule) that match on
certain criteria and perform various actions.

The rules themselves use a combination of logical operators matching specific keys on
the input JSON and operators.

## Motivation

Existing DICOM classification in Flywheel relies on using hard-coded rules and heuristics
principally using DICOM tags like `SeriesDescription` and `ProtocolName` to classify a
DICOM. This has many drawbacks including the difficulty to change classification rules.

This library provides a generalized declarative metadata modification that can
perform classification on multiple filetypes and both inside and outside flywheel

## Installation

`fw-classification` requires python >= 3.8, and can be installed via pip ``pip install
fw-classification``, or your favorite dependency manager, such as poetry ``poetry add
fw-classification``.

### Extras

There are a number of extras that can be optionally installed with ``pip install
"fw-classification[<extra>]"``, or ``poetry add -E <extra> fw-classification``.  These
extras extend the functionality of fw-classification and are mostly used by the
[Adapters](./fw-classification/adapters.md)  Available extras are:

* `all`: Includes both of the following
* `fw`: Includes [fw-core-client](https://gitlab.com/flywheel-io/tools/lib/fw-core-client)
  * Used in the [`FWAdapter`](./fw-classification/adapters.md#fwadapter) and
    [`NiftiFWAdapter`](./fw-classification/adapters.md#niftifwadapter) for getting
    either the hierarchy, or the sidecar, respectively.
* `geartk`: Includes [flywheel-gear-toolkit](https://gitlab.com/flywheel-io/public/gear-toolkit)

  * Used in the [`FWAdapter`](./fw-classification/adapters.md#fwadapter) and
    [`NiftiFWAdapter`](./fw-classification/adapters.md#niftifwadapter) for updating
    metadata via the gear.
