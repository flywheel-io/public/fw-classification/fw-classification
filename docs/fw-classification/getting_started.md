# Getting Started

## Basic Classification

Classification can be run either via the command line utility or by running through python

### Command Line

The command line can either run classification or validate a profile:

{{asciinema("classify_cli.rec", rows=20)}}

### Python package

You can run classification by using the function `fw_classification.classify.run_classification`:

**Ex.**

```python

from fw_classification.classify import run_classification
from pathlib import Path
import json

input_data = {}
with open('/path/to/input','r') as fp:
    input_data = json.load(fp)

profile_path = Path('./classification-profiles/profiles/main.yaml')
result, out_data = run_classification(profile_path, input_data)
```

## Examples

### Dicom adapter

```python title="classify.py"

from fw_classification.adapters.dicom import DicomAdapter
from pathlib import Path
import json

dcm_file = Path(
    "~/Downloads/T1w.dcm.zip"
).expanduser()

profile_path = Path('~/Documents/MR.yaml')
dcm_adapter = DicomAdapter(dcm_file)
out = dcm_adapter.classify(profile_path)
print(json.dumps(out, indent=2))
```

Then running the file:

{{asciinema("classify_py.rec", rows=10)}}
