#!/usr/bin/env bash

echo -ne "\e[1;32m❯\e[0m "
sleep 1
echo "python classify.py"
sleep 0.2

dir=$(dirname "$0")
python "$dir"/classify.py
